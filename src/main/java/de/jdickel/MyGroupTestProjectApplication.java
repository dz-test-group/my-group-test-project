package de.jdickel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyGroupTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyGroupTestProjectApplication.class, args);
	}

}

